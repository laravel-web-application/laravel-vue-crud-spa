<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

#### Things to do list:

1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-vue-crud-spa.git`
2. Go inside the folder: `cd laravel-vue-crud-spa`
3. Run `composer install`
4. Run `php artisan key:generate`
5. Run `cp .env.example .env` then set your DB credentials on that file
6. Run `php artisan migrate`
8. Run `npm install && npm run watch`
9. Run `php artisan serve`
10. Open your favorite browser: http://localhost:8000/category

### Images Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Add New Product

![Add New Product](img/add.png "Add New Product")

Edit & Update Product

![Edit & Update Product](img/update.png "Edit & Update Product")
